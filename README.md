# Welcome to our project

> #### After you clone the project, you will need to do some basic config:
>
> - composer install
> - cp .env.example .env (So you can copy your rnv example to make the env file)
> - php artisan key:generate
>

#### Install sail:

>
> - composer require laravel/sail --dev
> - alias sail='bash vendor/bin/sail' (Make an alias, so you don't need to run the whole sail path)
> - sail up and sail down awaaaaay - It is that easy
>
> #### Sail gives us a docker container with all we need, its important to remember to run commands inside of the container. We just need to do sail shell and work in there.
> 

